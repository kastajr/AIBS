#!/bin/bash

set -u
set -p

WORKDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
OUTPUT_FILE=$WORKDIR/"users_credentials.out"

function echoerr() {
  echo "[ERROR] $@"
}
function echosucc() {
  echo "[SUCCESS] $@"
}

function get_file_content () {
  local SOURCE_FILE=$1
  if [[ -f $WORKDIR/$SOURCE_FILE ]] ;then
    mapfile -t ARRAY_OF_USERS < $WORKDIR/$SOURCE_FILE
    echosucc "CREDENTIALS LOADED FROM $SOURCE_FILE"
  else
    echoerr "CREDENTIALS SOURCE FILE NOT FOUND"
  fi
}

function add_user () {
  local USERNAME=$1
  local PASSWORD=$2
  echo -e "$USERNAME $PASSWORD" >> $OUTPUT_FILE
  useradd -m -s /bin/bash -p $(openssl passwd -crypt $PASSWORD) $USERNAME 
  if [[ $? -ne 0 ]]; then
    echoerr "$USERNAME ADDITION FAILED."
  else
    echosucc "$USERNAME ADDED."
  fi
}

function create_random_users () {
  for (( i = 0; i < $1 ; i++ )); do
    local USERNAME="user_"$(openssl rand -hex 2) 
    local PASSWORD=$(openssl rand -base64 6)
    add_user $USERNAME $PASSWORD
  done
}

function create_users_from_file () {
  for ((i = 0; i < ${#ARRAY_OF_USERS[@]}; i++)); do
      local ROW="${ARRAY_OF_USERS[$i]}"
      local USERNAME=$(echo $ROW | cut -d ' ' -f1 )
      local PASSWORD=$(echo $ROW | cut -d ' ' -f2 )
      add_user $USERNAME $PASSWORD
  done
}

while getopts "rfh" OPTION; do
  case $OPTION in
  r)
      echo "[MODE]   CREATE NUMBER OF RANDOM ACCOUNTS"
      read -p "[PROMPT] ENTER THE NUMBER OF ACCOUNTS TO BE CREATED: " NUMBER_OF_ACCOUNTS
      if [[ $NUMBER_OF_ACCOUNTS =~ ^-?[0-9]+$ ]]; then
        create_random_users $NUMBER_OF_ACCOUNTS
        echosucc "RANDOM ACCOUNTS CREATED."
      else 
        echoerr "YOU HAVE TO PROVIDE A NUMBER."
      fi
      ;;
  f)
      echo "[MODE]:FROM FILE"
      if [[ ! $# -eq 2 ]]; then
        echoerr "YOU HAVE TO PROVIDE A PATH TO SOURCE FILE WITH CREDENTIALS."
      else 
        get_file_content $2
        create_users_from_file
        echosucc "ACCOUNTS CREATED."
      fi
      ;;
  h|?)
      echo "$0 -[frh] optional_path"
      echo "[-f] - create user accounts from file - you have to provide path"
      echo "[-r] - create given number of user accounts"
      echo "Script creates output file named users_credentials.out"
      ;;
  esac
done